Source: bloscpack
Section: utils
Priority: optional
Maintainer: Daniel Stender <stender@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 python3,
 python3-setuptools,
 python3-blosc,
 python3-six,
 python3-numpy,
 python3-nose <!nocheck>,
 python3-mock <!nocheck>
Standards-Version: 4.4.1
Homepage: https://github.com/Blosc/bloscpack
Vcs-Git: https://salsa.debian.org/python-team/packages/bloscpack.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/bloscpack

Package: bloscpack
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends}
Description: CLI utility for the Blosc metacompressor
 This is the command line interface utility for the Blosc metacompressor.
 It comes with native support for efficiently serializing and deserializing
 Numpy arrays, and a versatile Python 3 API.
 .
 Since Bloscpack is under development please regard that the internal
 storage format for the compressed files might change in the future, so
 do not depend critically on the generated files yet keeping on to be
 valid for upcoming versions.