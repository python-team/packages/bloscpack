#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim :set ft=py:

__version__ = '0.15.0'
__author__ = 'Valentin Haenel <valentin@haenel.co>'
